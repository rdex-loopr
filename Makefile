all: rdex-loopr

clean:
	-rm rdex-loopr

rdex-loopr: rdex-loopr.c
	gcc -std=c99 -Wall -Wextra -pedantic -O3 -march=native -o rdex-loopr rdex-loopr.c `pkg-config gsl --libs`

.PHONY: all clean
