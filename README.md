# rdex-loopr

Mining for time crystals: repeating patterns in reaction-diffusion systems.

## Prerequisites

GNU Scientific Library, FFMPEG, as well as C build tools.

    sudo apt install libgsl-dev ffmpeg, build-essential

## Compile

    make

## Run

    mkdir output
    cd output
    ../rdex-loopr a &
    ../rdex-loopr b &
    ...

If you have lots of cores and RAM:

    parallel -u ../rdex-loopr ::: A B C D E F G H

## Edit

Change `S` to the desired size.  80 needs 7.5GB RAM, O(S^4).

## Legal

rdex-loopr (C) 2018 Claude Heiland-Allen <claude@mathr.co.uk>

Copyleft: This is a free work, you can copy, distribute, and
modify it under the terms of the Free Art License
<http://artlibre.org/licence/lal/en/>.
